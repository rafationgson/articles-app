import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { AuthorEntity } from 'src/author/entities/author.entity';
import { ArticleService } from './article.service';
import { CreateArticleDTO, FilterArticleDTO } from './dto/article.dto';
import { ArticleEntity } from './entities/article.entity';

const andWhereSpy = jest.fn().mockReturnThis();
const mockArticle: ArticleEntity = plainToClass(ArticleEntity,{id: "1", title: 'test article', body: 'article body',description:'desc', tags: ['tags']})
const mockAuthor: AuthorEntity = plainToClass(AuthorEntity, {id: 1, penName: 'John Doe'});

const mockArticleRepository = () => ({
  createQueryBuilder: jest.fn(()=>({
    leftJoinAndSelect: jest.fn().mockReturnThis(),
    update: jest.fn().mockReturnThis(),
    andWhere: andWhereSpy,
    where: jest.fn().mockReturnThis(),
    getOne: jest.fn().mockResolvedValue(mockArticle),
    getMany: jest.fn().mockReturnValueOnce(Promise),
    set: jest.fn().mockReturnThis(),
    execute: jest.fn().mockResolvedValue('updated article')
  })),
  create: jest.fn().mockResolvedValue(mockArticle),
  save: jest.fn().mockReturnValue('new task'),
  findOne: jest.fn().mockReturnValue(mockAuthor),
  delete: jest.fn()
})

const mockAuthorRepository = () => ({
  findOne: jest.fn().mockResolvedValue(mockAuthor)
})

describe('ArticleService', () => {
  let service: ArticleService;
  let articleRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        {provide: getRepositoryToken(ArticleEntity), useFactory: mockArticleRepository},
        {provide: getRepositoryToken(AuthorEntity), useFactory: mockAuthorRepository}
        
      ],
    }).compile();

    service = module.get<ArticleService>(ArticleService);
    articleRepository = module.get(getRepositoryToken(ArticleEntity));
  });

  it('get list of articles with filter options', () => {
    expect(articleRepository.createQueryBuilder).not.toHaveBeenCalled();
    const filters: FilterArticleDTO = {search:'beach',filterByTags: 'summer'};
    service.getListofArticles(filters)
    expect(articleRepository.createQueryBuilder).toHaveBeenCalled();
    expect(andWhereSpy).toHaveBeenCalledTimes(2);
  });

  it('get one article with ID',async ()=>{
    expect(articleRepository.createQueryBuilder).not.toHaveBeenCalled();
    const id = "1";
    const result = await service.getOneArticle(id);
    expect(result).toEqual(mockArticle);
  })

  it('throw error with article id that does not exist', async ()=>{
    expect(articleRepository.createQueryBuilder).not.toHaveBeenCalled();
    const id = "2";
    try {
      const result = await service.getOneArticle(id);
    } catch (error) {
      expect(error).toBeInstanceOf(new NotFoundException('Article does not exist')); 
    }
  })

  it('create article', async ()=>{
    expect(articleRepository.create).not.toHaveBeenCalled();
    
    const createDTO: CreateArticleDTO = {title: 'Test',body:'test body',description:'test desc', tags:['tags'], authorID: 1}
    const result = await service.createArticle(createDTO);
    expect(articleRepository.create).toHaveBeenCalledWith(createDTO);
    expect(result).toEqual('new task');
  })
  it('delete article', async ()=>{
    articleRepository.delete.mockResolvedValue({affected: 1});
    expect(articleRepository.delete).not.toHaveBeenCalled();
    await service.deleteArticle('1');
    expect(articleRepository.delete).toHaveBeenCalledWith(Number('1'));
  })

  it('throw an error if deleting an article that does not exist', ()=>{
    articleRepository.delete.mockResolvedValue({affected: 0});
    expect(articleRepository.delete).not.toHaveBeenCalled();
    expect(service.deleteArticle('1')).rejects.toThrow(NotFoundException);
  })
});
