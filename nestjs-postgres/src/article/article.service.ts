import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthorEntity } from 'src/author/entities/author.entity';

import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { CreateArticleDTO, FilterArticleDTO, CustomUpdateObject, UpdateArticleDTO } from './dto/article.dto';
import { ArticleEntity } from './entities/article.entity';
import {
    paginate,
    Pagination,
    IPaginationOptions,
  } from 'nestjs-typeorm-paginate';

@Injectable()
export class ArticleService {
    constructor(
        @InjectRepository(ArticleEntity) private articleReposository: Repository<ArticleEntity>,
        @InjectRepository(AuthorEntity) private authorRepository: Repository<AuthorEntity>
        ) {

    }

    async getListofArticles(filterDTO: FilterArticleDTO,options: IPaginationOptions): Promise<Pagination<ArticleEntity>> {
        const {search, filterByTags} = filterDTO;
        const queryBuilder = this.articleReposository.createQueryBuilder('articles').leftJoinAndSelect('articles.author','authors');
        if (search) {
            queryBuilder.andWhere("LOWER(articles.title) LIKE :search OR LOWER(articles.description) LIKE :search OR LOWER(authors.penName) LIKE :search",{ search: `%${search.toLowerCase()}%`})
        }
        if (filterByTags) {

            
            queryBuilder.andWhere("articles.tags && ARRAY[:...tags]",{tags: filterByTags});
        } 
        queryBuilder.orderBy('articles.id');

        return paginate<ArticleEntity>(queryBuilder, options)
    }

    async createArticle(articleDetails: CreateArticleDTO): Promise<ArticleEntity> {
        const {authorID} = articleDetails;
        const author = await this.findAuthor(authorID);
        const newArticle = this.articleReposository.create(articleDetails);
        newArticle.author = author;
        return await this.articleReposository.save(newArticle);
    }

    async getOneArticle(id: string): Promise<ArticleEntity> {
        const article = await this.articleReposository.createQueryBuilder('articles').leftJoinAndSelect('articles.author','authors').where("articles.id =:id",{id}).getOne();
        if (!article) {
            throw new NotFoundException('Article does not exist');
        }
        return article;
    }

    async updateArticle(id: string, articleDetails: UpdateArticleDTO) {
        if (Object.keys(articleDetails).length < 1) {
            throw new BadRequestException('Empty request body');
        }
        const {authorID, title, description, body, tags } = articleDetails;
        const query = this.articleReposository.createQueryBuilder('articles').update()
        const updates: CustomUpdateObject = {};
        if (authorID) {
            const author = await this.findAuthor(authorID);
            updates.author = author;
        }
        if (title){
            updates.title = title;
        }
        if (description){
            updates.description = description;
        }
        if (body){
            updates.body = body;
        }
        if (tags) {
            updates.tags = tags;
        }
        query.set(updates);
        const result =  await query.where("articles.id =:id",{id}).execute();
        if (result.affected < 1) {
            throw new NotFoundException('Article does not exist');
        }
        const updatedArticle = await this.getOneArticle(id);
        return updatedArticle;
    }

    async deleteArticle(id: string): Promise<DeleteResult> {
        const result = await this.articleReposository.delete(Number(id));
        if (result.affected < 1) {
            throw new NotFoundException('Article does not exist');
        }
        return result;
    }

    private async findAuthor(authorID: number): Promise<AuthorEntity> {
        const author = await this.authorRepository.findOne({where: {id:authorID}});
        if (!author) {
            throw new NotFoundException('Author does not exist');
        }
        return author;
    }
}
