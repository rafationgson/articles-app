import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Pagination } from 'nestjs-typeorm-paginate';
import { ArticleService } from './article.service';
import { CreateArticleDTO, FilterArticleDTO, UpdateArticleDTO } from './dto/article.dto';
import { ArticleEntity } from './entities/article.entity';

@Controller('articles')
@UseGuards(AuthGuard('jwt'))
export class ArticleController {
    constructor(private articleService: ArticleService){}

    @Get()
    @UsePipes(new ValidationPipe({transform: true}))
    async getListofArticles(@Query() filterDTO: FilterArticleDTO, @Query('page') page: number = 1, @Query('limit') limit: number = 5): Promise<Pagination<ArticleEntity>>{
        console.log(filterDTO.filterByTags);
             
        return this.articleService.getListofArticles(filterDTO,{page,limit});
    }

    @Get(':id')
    async getOneArticle(@Param('id',ParseIntPipe) id: string) {
        const oneArticle = await this.articleService.getOneArticle(id);        
        return {
            article: {
                ...oneArticle
            }
        }
    }

    @Post()
    async createArticle(@Body(ValidationPipe) articleDetails: CreateArticleDTO) {
        const newArticle = await this.articleService.createArticle(articleDetails);
        return {
            article: {
                ...newArticle
            }
        }
    }

    @Put(':id')
    async updateArticle(@Param('id',ParseIntPipe) id: string, @Body(ValidationPipe) articleDetails: UpdateArticleDTO) {
        const updatedArticle = await this.articleService.updateArticle(id, articleDetails);
        return {
            article: {
                ...updatedArticle
            }
        }
    }

    @Delete(':id')
    async deleteArticle(@Param('id',ParseIntPipe) id: string) {
        await this.articleService.deleteArticle(id);
        return {
            success: true,
            message: "Sucessfully deleted"
        }
    }
}
