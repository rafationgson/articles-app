import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleEntity } from './entities/article.entity';
import { AuthModule } from 'src/auth/auth.module';
import { AuthorModule } from 'src/author/author.module';

@Module({
  imports: [TypeOrmModule.forFeature([ArticleEntity]), AuthModule, AuthorModule],
  providers: [ArticleService],
  controllers: [ArticleController]
})
export class ArticleModule {}
