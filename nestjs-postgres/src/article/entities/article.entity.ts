import { classToPlain } from "class-transformer";
import { AuthorEntity } from "src/author/entities/author.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('articles')
export class ArticleEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    body: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column('text', {array: true})
    tags: string[];

    @ManyToOne(() => AuthorEntity, author => author.articles,  {onDelete: 'CASCADE'})
    author: AuthorEntity;

    toJSON(){
        return classToPlain(this);
    }
}