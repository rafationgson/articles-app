import { Transform } from "class-transformer";
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateArticleDTO {
    @IsNotEmpty()
    @IsString()
    title: string;
    
    @IsNotEmpty()
    @IsString()
    description: string;
    
    @IsNotEmpty()
    @IsString()
    body: string;

    @IsNotEmpty()
    @IsNumber()
    authorID: number;
    
    @IsArray()
    @ArrayNotEmpty()
    @IsString({each: true})
    @IsNotEmpty({each:true})
    tags: string[];
}

export class UpdateArticleDTO {
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    title: string;
    
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    description: string;
    
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    body: string;

    @IsOptional()
    @IsNotEmpty()
    @IsNumber()
    authorID: number;

    @IsOptional()
    @IsArray()
    @ArrayNotEmpty()
    @IsString({each: true})
    @IsNotEmpty({each:true})
    tags: string[];
}

export class FilterArticleDTO {
    @IsOptional()
    search: string;
    
    @IsOptional()
    @Transform(value => {
        if (!Array.isArray(value.value)){
            return [value.value]
        } else {
            return value.value
        }
    })
    filterByTags: string[];
}

export interface CustomUpdateObject {
    [key: string]: any
}