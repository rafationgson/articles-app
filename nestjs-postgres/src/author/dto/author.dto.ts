import { IsNotEmpty, IsString } from "class-validator";

export class SearchAuthorDTO {
    search: string;
}

export class AuthorDTO {
    @IsNotEmpty()
    @IsString()
    penName: string;
}