import { Module } from '@nestjs/common';
import { AuthorService } from './author.service';
import { AuthorController } from './author.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorEntity } from './entities/author.entity';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([AuthorEntity]),AuthModule],
  providers: [AuthorService],
  controllers: [AuthorController],
  exports: [TypeOrmModule]
})
export class AuthorModule {}
