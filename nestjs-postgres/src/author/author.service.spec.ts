import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { AuthorService } from './author.service';
import { AuthorDTO, SearchAuthorDTO } from './dto/author.dto';
import { AuthorEntity } from './entities/author.entity';

const mockArticleRepository = () => ({
  createQueryBuilder: jest.fn(),
  findOne: jest.fn(),
  create: jest.fn(),
  save: jest.fn(),
  update: jest.fn(),
  delete: jest.fn()
})

describe('AuthorService', () => {
  let service: AuthorService;
  let authorRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthorService,
        {provide: getRepositoryToken(AuthorEntity), useFactory: mockArticleRepository}
      ],
    }).compile();

    service = module.get<AuthorService>(AuthorService);
    authorRepository = module.get(getRepositoryToken(AuthorEntity))
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('get all authors', ()=>{
    expect(authorRepository.createQueryBuilder).not.toHaveBeenCalled();
    const searchDTO: SearchAuthorDTO = {search: 'john'}
    service.getListOfAuthors(searchDTO);
    expect(authorRepository.createQueryBuilder).toHaveBeenCalled();
  })

  it('get one author',async()=>{
    const mockAuthor: AuthorEntity = plainToClass(AuthorEntity, {id: 1, penName: 'John Doe'});
    authorRepository.findOne.mockResolvedValue(mockAuthor);
    expect(authorRepository.findOne).not.toHaveBeenCalled();
    const result = await service.getOneAuthor('1');
    expect(result).toEqual(mockAuthor);
  })

  it('throw error with authorId that does not exist',async()=>{
    const mockAuthor: AuthorEntity = plainToClass(AuthorEntity, {id: 1, penName: 'John Doe'});
    authorRepository.findOne.mockResolvedValue(mockAuthor);
    expect(authorRepository.findOne).not.toHaveBeenCalled();
    

    try {
      await service.getOneAuthor('2');
    } catch (error) {
      expect(error).toBeInstanceOf(new NotFoundException('Author does not exist'));
    }
  })

  it('create author', async ()=>{
    expect(authorRepository.create).not.toHaveBeenCalled();

    const createAuthorDTO: AuthorDTO = {penName: 'John Doe'}
    const result = await service.createAuthor(createAuthorDTO);
    expect(authorRepository.create).toHaveBeenCalledWith(createAuthorDTO);
  })
  
  it('update author', async ()=>{
    authorRepository.update.mockResolvedValue({affected: 1});
    expect(authorRepository.update).not.toHaveBeenCalled();
    const updateAuthor: AuthorDTO = {penName: 'John'};
    await service.updateAuthor('1', updateAuthor);
    expect(authorRepository.update).toHaveBeenCalledWith('1',updateAuthor);
  })

  it('throw and error when updating an author that does not exist', async ()=>{
    authorRepository.update.mockResolvedValue({affected: 0});
    expect(authorRepository.update).not.toHaveBeenCalled();
    const updateAuthor: AuthorDTO = {penName: 'John'};
    expect(service.updateAuthor('1', updateAuthor)).rejects.toThrow(NotFoundException);
    
  })

  it('delete author', async ()=>{
    authorRepository.delete.mockResolvedValue({affected: 1});
    expect(authorRepository.delete).not.toHaveBeenCalled();
    await service.deleteAuthor('1');
    expect(authorRepository.delete).toHaveBeenCalledWith('1');
  })

  it('throw and error when deleting an author that does not exist', async ()=>{
    authorRepository.delete.mockResolvedValue({affected: 0});
    expect(authorRepository.delete).not.toHaveBeenCalled();
    expect(service.deleteAuthor('1')).rejects.toThrow(NotFoundException);
  })
});
