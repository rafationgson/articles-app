import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { AuthorDTO, SearchAuthorDTO } from './dto/author.dto';
import { AuthorEntity } from './entities/author.entity';
import {
    paginate,
    Pagination,
    IPaginationOptions,
  } from 'nestjs-typeorm-paginate';

@Injectable()
export class AuthorService {
    constructor(@InjectRepository(AuthorEntity) private authorRepository: Repository<AuthorEntity>) {}

    async getListOfAuthors(searchDTO: SearchAuthorDTO, options: IPaginationOptions): Promise<Pagination<AuthorEntity>> {
        const {search} = searchDTO;
        const query = this.authorRepository.createQueryBuilder('authors');
        if (search) {
            query.andWhere('LOWER(authors.penName) LIKE :name',{name: `%${search.toLowerCase()}%` });
        }
        return paginate<AuthorEntity>(query,options);
    }

    async getOneAuthor(id: string): Promise<AuthorEntity> {
        const result = await this.authorRepository.findOne({where: { id }});
        if (!result) {
            throw new NotFoundException('Author does not exist');
        }
        return result;
    }

    async createAuthor(createAuthor: AuthorDTO): Promise<AuthorEntity> {
        const newAuthor = this.authorRepository.create(createAuthor);
        return await this.authorRepository.save(newAuthor);
        
    }

    async updateAuthor(id: string, updateAuthor: AuthorDTO): Promise<AuthorEntity> {
        const result = await this.authorRepository.update(id, updateAuthor);
        if (result.affected < 1) {
            throw new NotFoundException('Author does not exist');
        }
        const updatedAuthor = await this.getOneAuthor(id);
        return updatedAuthor;
    }

    async deleteAuthor(id:string): Promise<DeleteResult> {
        const result = await this.authorRepository.delete(id);
        if (result.affected < 1) {
            throw new NotFoundException('Author does not exist');
        }
        return result;
    }
}
