import { classToPlain, Exclude } from 'class-transformer';
import { ArticleEntity } from 'src/article/entities/article.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('authors')
export class AuthorEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique: true})
    penName: string;

    @OneToMany(() => ArticleEntity, article => article.author, {eager: true, onDelete: 'CASCADE', onUpdate:'CASCADE'})

    articles: ArticleEntity[];

    toJSON(){
        return classToPlain(this);
    }
    
}