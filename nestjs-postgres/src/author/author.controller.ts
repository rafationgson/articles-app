import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthorService } from './author.service';
import { AuthorDTO, SearchAuthorDTO } from './dto/author.dto';

@Controller('author')
@UseGuards(AuthGuard('jwt'))
export class AuthorController {
    constructor(private authorService: AuthorService){}

    @Get()
    async getListOfAuthors(@Query() searchDTO: SearchAuthorDTO, @Query('page') page: number = 1,@Query('limit') limit: number = 5){
        return this.authorService.getListOfAuthors(searchDTO,{page,limit});
        
    }

    @Get(':id')
    async getOneAuthor(@Param('id', ParseIntPipe) id: string){
        const author = await this.authorService.getOneAuthor(id); 
        return {
            author: {
                ...author
            }
        }
    }

    @Post()
    createAuthor(@Body(ValidationPipe) createAuthor: AuthorDTO) {
        return this.authorService.createAuthor(createAuthor);
    }

    @Put(':id')
    async updateAuthor(@Param('id', ParseIntPipe) id: string, @Body(ValidationPipe) updateAuthor: AuthorDTO) {
        const updatedAuthor = await this.authorService.updateAuthor(id,updateAuthor);
        return {
            author: {
                ...updatedAuthor
            }
        }
    }

    @Delete(':id')
    async deleteAuthor(@Param('id', ParseIntPipe) id: string) {
        await this.authorService.deleteAuthor(id);
        return {
            success: true,
            message: "Sucessfully deleted"
        }
    }
}
