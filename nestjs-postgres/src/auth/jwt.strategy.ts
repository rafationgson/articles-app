import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { passportJwtSecret } from "jwks-rsa";
import { Strategy, VerifiedCallback } from "passport-jwt";
import { ExtractJwt } from "passport-jwt";
import * as dotenv from 'dotenv';
dotenv.config();

@Injectable()
export class AuthJwtStrategy extends PassportStrategy(Strategy){
    constructor() {
        super({
            secretOrKeyProvider: passportJwtSecret({
            cache: true,
            rateLimit: true,
            jwksRequestsPerMinute: 5,
            jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`,
            }),
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            audience: process.env.AUTH0_AUDIENCE,
            issuer: `https://${process.env.AUTH0_DOMAIN}/`,
            algorithms: ['RS256'],
        });
    }

    validate(payload: any, done: VerifiedCallback) {
        
        if (!payload) {
            throw new UnauthorizedException();
        }
        return done(null, payload);
    }
}