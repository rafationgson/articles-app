import { IsEmail, IsNotEmpty, IsString, Matches, MaxLength, MinLength } from 'class-validator';

export class LoginDTO {
    @MaxLength(20)
    @MinLength(4)
    @IsString()
    username: string

    @IsString()
    @MaxLength(20)
    @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/, {message: 'Password is too weak'})
    password: string
}

export class RegistrationDTO extends LoginDTO {
    @IsEmail()
    @IsString()
    email: string
}

export interface AuthPayload {
    username: string;
}
